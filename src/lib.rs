pub struct DisjointSet {
   parent: Vec<usize> 
}

impl DisjointSet {
    pub fn make_set(&mut self) {
        self.parent.push(self.parent.len());
    }

    pub fn find(&mut self, x: usize) -> usize {
        let mut current = x;
        let mut next = self.parent[current];
        while current != next {
            current = next;
            next = self.parent[next];
        }
        self.parent[x] = current;
        current
    }

    pub fn union(&mut self, x: usize, y: usize) {
        let x_root = self.find(x);
        let y_root = self.find(y);
        if x_root == y_root {
            return
        }
        self.parent[y_root] = x_root
    }
}

#[cfg(test)]
mod unit_tests {
    use super::*;

    fn create_dsu() -> DisjointSet {
        let v = Vec::new();
        DisjointSet{ parent: v }
    }

    #[test]
    fn test_make_set() {
        let mut dsu = create_dsu();
        dsu.make_set();
        assert_eq!(dsu.parent.len(), 1);
    }

    #[test]
    fn test_find() {
        let mut dsu = create_dsu();
        for i in 0..2 {
            dsu.parent.push(i);
        }
        dsu.parent[1] = 0;
        assert_eq!(dsu.find(1), 0);
    }

    #[test]
    fn test_find_itself() {
        let mut dsu = create_dsu();
        dsu.parent.push(0);
        assert_eq!(dsu.find(0), 0);
    }

    #[test]
    fn test_find_path_compression() {
        let mut dsu = create_dsu();
        for i in 0..3 {
            dsu.parent.push(i);
        }
        dsu.parent[2] = 1;
        dsu.parent[1] = 0;
        dsu.find(2);
        assert_eq!(dsu.parent[2], 0);
    }

    #[test]
    fn test_union() {
        let mut dsu = create_dsu();
        for i in 0..2 {
            dsu.parent.push(i);
        }
        dsu.union(0, 1);
        assert_eq!(dsu.parent[1], 0);
    }

    #[test]
    fn test_union_same_root() {
        let mut dsu = create_dsu();
        for i in 0..2 {
            dsu.parent.push(i);
        }
        dsu.parent[1] = 0;
        dsu.union(0, 1);
        assert_eq!(dsu.parent[1], 0);
    }
}
